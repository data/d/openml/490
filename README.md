# OpenML dataset: hip

https://www.openml.org/d/490

## Structure

The dataset has the following file structure:

* `dataset/`
  * `tables/`
    * [`data.csv`](./dataset/tables/data.csv): CSV file with data
    * [`data.pq`](./dataset/tables/data.pq): Parquet file with data
  * [`metadata.json`](./dataset/metadata.json): OpenML description of the dataset
  * [`features.json`](./dataset/features.json): OpenML description of table columns
  * [`qualities.json`](./dataset/qualities.json): OpenML qualities (meta-features)

## Description

**Author**: cc@maths.bath.ac.uk  
**Source**: [StatLib](http://lib.stat.cmu.edu/datasets/) - Date unknown  
**Please cite**:   

This is the hip measurement data from Table B.13 in Chatfield's Problem Solving (1995, 2nd edn, Chapman and Hall). It is given in 8 columns. First 4 columns are for Control Group. Last 4 columns are for Treatment group (Note there is no pairing. Patient 1 in Control Group is NOT patient 1 in Treatment Group).

## Contributing

This is a [read-only mirror](https://gitlab.com/data/d/openml/490) of an [OpenML dataset](https://www.openml.org/d/490). Contribute any changes to the dataset there. Alternatively, [fork the dataset](https://gitlab.com/data/d/openml/490/-/forks/new) or [find an existing fork](https://gitlab.com/data/d/openml/490/-/forks) to contribute to.

You can use [issues](https://gitlab.com/data/d/openml/490/-/issues) to discuss the dataset and any issues.

For more information see [https://datagit.org/](https://datagit.org/).

